# Design

## Functionality

- default parameters at `STRO` level, i.e. the parameters passed to `CoralType.deco()`
    - the use case is that the same kind of higher level type constructor may accept more or less parameters, and `CoralType` unions them all, therefore one has to define default parameters for those TRS that accepts less parameters
    - e.g. `ch.LowCardinality(value_type)` vs `pa.Dictionary(index_type, value_type, ordered)`
- default parameters at `CTRO` level, i.e. the explicitly-set default parameters of a CTRO underlying function. e.g.:

```python title="Examples" linenums="1" hl_lines="36 43 44 45"
from pydantic import BaseModel

class MorphParams(BaseModel):
    ...

class TRO(BaseModel):
    ...

class STRO(TRO):
    ...

class CTRO(TRO):
    ...

class CoralType(STRO):
    ...

class ChType(CTRO):
    ...

@CoralType
class CoralInt(MorphParams):
    ...

@CoralType
class CoralString(MorphParams):
    ...

@CoralType
class CoralDict(MorphParams):
    index_type: TRO
    value_type: TRO
    ordered: bool

@CoralInt.deco(tro_cls=ChType)
def ChInt(bit_len: int = 64, signed: bool = True):  # (1)
    ...

@CoralString.deco(tro_cls=ChType)
def ChString():
    ...

@CoralDict.deco(tro_cls=ChType,
                index_type=ChInt(bit_len=32, signed=True),
                ordered=False)  # (2)
def ChLowCardinality(value_type: ChType = ChString):  # (3)
    ...

```

1. When casting `ChInt` to a level 1 TRO, the provided `#!python bit_len=64` & `#!python signed=True` parameters will be used, i.e.: `#!python ChInt.cast() = ChInt(bit_len=64, signed=True)`

2. When casting  , The `STRO` level default parameters: `#!python index_type=ChInt32` & `#!python ordered=False`
3.

## Transformation

First we explore "half-transformation", i.e. `STRO -> CTRO`. For the sake of simplicity, we use `CoralType` as an example of `STRO`, and `ChType` as an example of `CTRO`.

Some premises:

- `CoralType` is the union of all supported types, hence every `type[STRO]` (e.g. `ChType`, `PaType`) instance must have a link to an instance of `CoralType`
    - Note that such instance of `CoralType` may very well be implicit, i.e. not explicitly written out as python code and added to the `CoralTypeReprSystem`.
- A transformation should produce a `CTRO` the same type level as the `STRO`, unless an explicit casting is requested.


### Logic

1. the STRO has direct & explicit corresponding CTRO (i.e. `ctro.semantic_type == stro`)
2. the STRO has no direct & explicit corresponding CTRO:
    1. `direct: True`, `explicit: False`:
    2. `direct: False`, `explicit: True`:
   3. `direct: False`, `explicit: False`:

```python title="Cases"
def transform(stro: CoralType) -> ChType:
    ...

ChTRS = TRS.build(nodes)

transform(CoralInt64) == ChInt64 and ChInt64 in ChTRS.nodes

ChUInt = ChInt(signed=False)
transform(CoralUInt) == ChUInt and ChUInt not in ChTRS.nodes


```

### Case 1:
