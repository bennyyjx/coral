# Coral

Coral is a utility library that safely & easily converts data types between a multitude of type representation system. The goal is to provide a unified abstraction of data types that underlies any Table / DataFrame like data structure. This includes:

- Relational databases: [PostgreSQL](https://www.postgresql.org/docs/current/datatype.html), [ClickHouse](https://clickhouse.com/docs/en/sql-reference/data-types) etc.
- PL agnostic data format: Apache Arrow, Apache Parquet etc.
- Python dataframe libraries: `pandas`, `numpy`, `pyarrow`, `polars`
- Other python libraries that wraps one/some of the above: `dagster`, `ibis`, `sqlalchemy` etc.

For more details, please refer to the online documentation or [serve it locally](#build-documentation-locally).

## Installation

```shell
pip install coral
```


## Development

### Prerequisites: `poetry`
The project uses [Poetry](https://python-poetry.org/) for packaging & dependency management. Follow the [official instructions](https://python-poetry.org/docs/#installation) to install poetry on your development machine.

### Setup

1. Clone the repo
2. Enter the repo directory
3. Run the following commands

```shell
# Creates a virtual environment within the repo directory
poetry init

# Installs all dependencies (including required dev dependencies) and the project itself into the virtual environment
poetry install
```

And you are ready to go.

### Build Documentation Locally

```shell
poetry install --with=docs
mkdocs serve
```





