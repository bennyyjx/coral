import dataclasses

from coral.builtin.trs.impls.clickhouse import ChStringImpl, ChBoolStrRepr


def test_module_import_success():
    from pydantic import ValidationError
    try:
        import coral.builtin.trs.impls.clickhouse
    except ValidationError as e:
        raise e
    except Exception as e:
        raise e


def test_descriptor():
    import coral.builtin.trs.impls.clickhouse as chimpl

    assert not chimpl.ChBoolStrRepr == "Bool"

    class Foo:
        a: ChStringImpl = chimpl.ChBoolStrRepr


    foo = Foo()
    assert foo.a == "Bool"
