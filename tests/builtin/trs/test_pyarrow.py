def test_module_import_success():
    from pydantic import ValidationError
    try:
        import coral.builtin.trs.pyarrow
    except ValidationError as e:
        raise e
    except Exception as e:
        raise e


def test_type_equivalence():
    from coral.builtin.trs.pyarrow import PaUInt8, PaInt
    PaUInt = PaInt(signed=False)
    PaUInt8_alt = PaUInt(bit_len=8)

    assert PaUInt8_alt == PaUInt8  # TODO: investigate pydantic modedl's __eq__ method, and its relationship with __hash__
    assert PaUInt != PaInt

