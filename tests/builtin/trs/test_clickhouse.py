def test_module_import_success():
    from pydantic import ValidationError
    try:
        import coral.builtin.trs.clickhouse
    except ValidationError as e:
        raise e
    except Exception as e:
        raise e


def test_type_equivalence():
    from coral.builtin.trs.clickhouse import ChInt, ChUInt8
    ChUInt = ChInt(signed=False)
    ChUInt8_alt = ChUInt(bit_len=8)

    assert ChUInt8_alt == ChUInt8  # TODO: investigate pydantic modedl's __eq__ method, and its relationship with __hash__
    assert ChUInt != ChInt

