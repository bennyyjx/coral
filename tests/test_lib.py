from typing import Sequence

import pytest

import pyarrow as pa
import coral.builtin.trs.clickhouse as ch
import coral.core.coral as cr
from coral.lib import transform_ch_to_pa, transform_cr_to_pa, transform_cr_to_ch

test_cases: dict[str, dict[str, Sequence]] = {
    'transform_ch_to_pa': {
        "expect_success": [
            ((ch.ChUInt8,), pa.uint8()),
            ((ch.ChUInt16,), pa.uint16()),
            ((ch.ChUInt32,), pa.uint32()),
            ((ch.ChUInt64,), pa.uint64()),
            ((ch.ChInt8,), pa.int8()),
            ((ch.ChInt16,), pa.int16()),
            ((ch.ChInt32,), pa.int32()),
            ((ch.ChInt64,), pa.int64()),
            ((ch.ChFloat32,), pa.float32()),
            ((ch.ChFloat64,), pa.float64()),
            ((ch.ChDateTime64(unit="ms", tz="Asia/Shanghai"),), pa.timestamp("ms", "Asia/Shanghai")),
            ((ch.ChString,), pa.string()),
        ],
        "expect_fail": [
            # (ch.ChFixedString,),
        ]
    },
    'transform_cr_to_pa': {
        "expect_success": [
            ((cr.CoralUInt8,), pa.uint8()),
            ((cr.CoralInt(bit_len=16, signed=False),), pa.uint16()),
            ((cr.CoralInt(bit_len=32, signed=False),), pa.uint32()),
            ((cr.CoralInt(bit_len=64, signed=False),), pa.uint64()),
            ((cr.CoralInt(bit_len=8, signed=True),), pa.int8()),
            ((cr.CoralInt(bit_len=16, signed=True),), pa.int16()),
            ((cr.CoralInt(bit_len=32, signed=True),), pa.int32()),
            ((cr.CoralInt(bit_len=64, signed=True),), pa.int64()),
            ((cr.CoralFloatNumber(bit_len=32),), pa.float32()),
            ((cr.CoralFloatNumber(bit_len=64),), pa.float64()),
            ((cr.CoralTimestamp(unit="ms", tz="Asia/Shanghai"),), pa.timestamp("ms", "Asia/Shanghai"))
        ],
        "expect_fail": [
        ]
    },
    'transform_cr_to_ch_cast_false': {
        "expect_success": [
            ((cr.CoralUInt8,), "UInt8"),
            ((cr.CoralInt(bit_len=16, signed=False),), "UInt16"),
            ((cr.CoralInt(bit_len=32, signed=False),), "UInt32"),
            ((cr.CoralInt(bit_len=64, signed=False),), "UInt64"),
            ((cr.CoralInt(bit_len=8, signed=True),), "Int8"),
            ((cr.CoralInt(bit_len=16, signed=True),), "Int16"),
            ((cr.CoralInt(bit_len=32, signed=True),), "Int32"),
            ((cr.CoralInt(bit_len=64, signed=True),), "Int64"),
            ((cr.CoralFloatNumber(bit_len=32),), "Float32"),
            ((cr.CoralFloatNumber(bit_len=64),), "Float64"),
            ((cr.CoralFixedNumber(precision=10, scale=3),), "Decimal(10, 3)"),
            ((cr.CoralFixedByteString(byte_len=5),), "FixedString(5)"),
            ((cr.CoralVarString(is_large=False),), "String"),
            ((cr.CoralTimestamp(unit="s", tz="Asia/Shanghai"),), "DateTime64(0, 'Asia/Shanghai')"),
            ((cr.CoralTimestamp(unit="ms", tz="Asia/Shanghai"),), "DateTime64(3, 'Asia/Shanghai')"),
            ((cr.CoralTimestamp(unit="us", tz="Asia/Shanghai"),), "DateTime64(6, 'Asia/Shanghai')"),
            ((cr.CoralTimestamp(unit="ns", tz="Asia/Shanghai"),), "DateTime64(9, 'Asia/Shanghai')"),
            ((cr.CoralDictionary(value_type=cr.CoralVarString),), "LowCardinality(String)"),
        ],
        "expect_fail": [],
    },
    'transform_cr_to_ch_cast_false_no_calling': {
        "expect_success": [
            ((cr.CoralInt,), ch.ChInt),
            ((cr.CoralUnsignedInt,), ch.ChInt(signed=False)),
            ((cr.CoralDate,), ch.ChDate32),
            ((cr.CoralTimestamp,), ch.ChDateTime64),
            ((cr.CoralVarString(is_large=False),), ch.ChString),
            ((cr.CoralFixedByteString,), ch.ChFixedString),
            ((cr.CoralDictionary,), ch.ChLowCardinality),
            ((cr.CoralDictionary(value_type=cr.CoralVarString),), ch.ChLowCardinality(value_type=ch.ChString)),
        ],
        "expect_fail": [],
    },
    'transform_cr_to_ch_cast_true': {
        "expect_success": [
            ((cr.CoralInt,), "Int64"),
            ((cr.CoralInt(signed=False),), "UInt64"),
            ((cr.CoralInt(bit_len=16),), "Int16"),
            ((cr.CoralFloatNumber,), "Float64"),
            ((cr.CoralVarString,), "String"),
            ((cr.CoralTimestamp,), "DateTime64(3, 'Asia/Shanghai')"),
            ((cr.CoralDictionary,), "LowCardinality(String)"),
        ],
        "expect_fail": [],
    },
}


@pytest.mark.parametrize(["input", "expected"], test_cases['transform_ch_to_pa']['expect_success'])
def test_transform_ch_to_pa(input, expected):
    assert transform_ch_to_pa(*input)() == expected


@pytest.mark.parametrize("input", test_cases['transform_ch_to_pa']['expect_fail'])
def test_transform_ch_to_pa_expect_fail(input):
    with pytest.raises(AssertionError):
        transform_ch_to_pa(*input)()


@pytest.mark.parametrize(["input", "expected"], test_cases['transform_cr_to_pa']['expect_success'])
def test_transform_cr_to_pa(input, expected):
    assert transform_cr_to_pa(*input)() == expected


@pytest.mark.parametrize(["input", "expected"], test_cases['transform_cr_to_ch_cast_false']['expect_success'])
def test_transform_cr_to_ch(input, expected):
    assert transform_cr_to_ch(*input)() == expected


@pytest.mark.parametrize(["input", "expected"], test_cases['transform_cr_to_ch_cast_false_no_calling']['expect_success'])
def test_transform_cr_to_ch_v2_no_calling(input, expected):
    assert transform_cr_to_ch(*input) == expected
