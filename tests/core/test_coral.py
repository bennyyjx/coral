import pytest

def test_module_import_success():
    from pydantic import ValidationError
    try:
        import coral.core.coral
    except ValidationError as e:
        raise e
    except Exception as e:
        raise e


def test_all_coral_types_are_distinctly_hashed():
    import coral.core.coral as cr

    coral_type_hashes = {}
    for name, obj in cr.__dict__.items():
        if isinstance(obj, cr.CoralType):
            coral_type_hashes[name] = hash(obj)
    try:
        assert len(set(coral_type_hashes.values())) == len(coral_type_hashes)
    except AssertionError as e:
        from itertools import groupby
        grouped_by_hash = {
            k: list(g)
            for k, g in groupby(coral_type_hashes.items(), key=lambda x: x[1])
        }
        from pprint import pprint
        pprint(grouped_by_hash)
        raise e
