[tool.poetry]
name = "coral"
version = "0.0.1"
description = "The hub for type system."
authors = ["Ben Yang <ben@ya.ng>"]
license = "MIT"
readme = "README.md"
keywords = [
    "Environment :: Console",
    "Operating System :: POSIX",
    "Operating System :: POSIX :: Linux",
    "Programming Language :: Python :: 3 :: Only",
    "Programming Language :: Python :: Implementation :: CPython",
    "Typing :: Typed"
]

[tool.poetry.dependencies]
python = "~3.10"
pydantic = "^1.10.7"
networkx = "^3.0"
pyarrow = "^11.0.0"
clickhouse-connect = "^0.5.16"
griffe = "^0.25.5"

[tool.poetry.group.dev.dependencies]
# linter & formatter
ruff = "^0.0.260"
isort = "^5.10.1"
black = "^22.6.0"
pydocstringformatter = "^0.6.2"

# type checking
mypy = "^1.0.0"

# task runner
poethepoet = "^0.16.0"

# for viewing networkx graph
bokeh = "^3.1.0"

[tool.poetry.group.docs]
optional = true

[tool.poetry.group.docs.dependencies]
mkdocs-material = "^8.3.9"
mkdocstrings = { extras = ["python"], version = "^0.20.0" }

[tool.poetry.group.test.dependencies]
# unit test
pytest = "^7.1.2"

[tool.pytest.ini_options]
### ref: https://docs.pytest.org/en/7.1.x/reference/reference.html#ini-options-ref
# minversion = "6.0"
# testpaths = [
#     "tests",
#     "integration",
# ]
# addopts = "-ra -q"
python_files = [
    "test_*.py",
    "check_*.py",
    "example_*.py"
]

[tool.mypy]
ignore_missing_imports = true

[tool.ruff]
line-length = 88
target-version = "py310"

select = [
    "E", # pycodestyle errors
    "W", # pycodestyle warnings
    "F", # pyflakes
    # "I",  # isort
    "C", # flake8-comprehensions
    "B", # flake8-bugbear
    "RUF", # Ruff-specific rules
]

ignore = [
    "E501", # line too long, handled by black
    "B008", # do not perform function calls in argument defaults
    "C901", # too complex
    "F401", # package imported but unused, # dev-only, should not be ignored in release
    "F841", # unused variable, # dev-only, should not be ignored in release
    "RUF001", # ambiguous unicode characters, unfriendly to CJK punctuations
    "RUF002", # ambiguous unicode characters, unfriendly to CJK punctuations
    "RUF003", # ambiguous unicode characters, unfriendly to CJK punctuations
]

extend-exclude = [
    ".debug"
]

[tool.black]
line-length = 88
target-version = ['py310']

[tool.isort]
profile = "black"
src_paths = ["coral", "tests"]

[tool.pydocstringformatter]
write = false
exclude = [
    ".venv/**",
    ".git/**",
    ".vscode/**",
    ".mypy_cache/**",
    ".pytest_cache/**",
    "dist/**",
]

[tool.tox]
# When tox has proper support for pyproject.toml, move all configurations here.
# Otherwise it's better to stay at tox.ini for clarity.

[tool.poe.tasks]
# tasks run by `poethepoet`

[tool.poe.tasks.format]
help = "Run black on the code base."
cmd = "black ."

[tool.poe.tasks.test]
help = "Run unit and feature tests."
cmd = "pytest"

[tool.poe.tasks.clean]
help = "Remove generated files"
cmd = """
    rm -rf .coverage
           .mypy_cache
           .pytest_cache
           ./**/__pycache__
           dist
           htmlcov
    """

[tool.poe.tasks.types-mypy]
help = "Run mypy type checker"
cmd = "mypy coral --ignore-missing-imports"

[tool.poe.tasks.lint]
help = "Run the linter"
cmd = "pylint coral"

[tool.poe.tasks.black-validate]
help = "Validate code style with black."
cmd = "black . --check --diff --color"

[tool.poe.tasks.black-format]
help = "Format the code with black."
cmd = "black ."

[tool.poe.tasks.isort-validate]
help = "Validate import sort order with isort."
cmd = "isort . --check --diff"

[tool.poe.tasks.isort-format]
help = "Format import sort order with isort."
cmd = "isort ."

[tool.poe.tasks.docstring-validate]
help = "Validate docstring style with pydocstringformatter."
cmd = "pydocstringformatter ."

[tool.poe.tasks.docstring-format]
help = "Format docstring with pydocstringformatter."
cmd = "pydocstringformatter --write ."

[tool.poe.tasks.style-validate]
help = "Validate code style with defined toolchain (black, isort, pydocstringformatter)."
sequence = [
    "black-validate",
    "isort-validate",
    "docstring-validate",
]

[tool.poe.tasks.style-format]
help = "Fix code style with defined toolchain (black, isort, pydocstringformatter)."
sequence = [
    "black-format",
    "isort-format",
    "docstring-format"
]



[build-system]
requires = ["poetry-core>=1.0.0"]
build-backend = "poetry.core.masonry.api"
