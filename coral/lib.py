from typing import TypeVar

import networkx as nx

from coral.core.coral import CoralType
from coral.core.repr import ConcreteTypeReprObject, SemanticTypeReprObject, MorphParams

from coral.builtin.trs.pyarrow import PyArrowTypeReprSystem, PaType
from coral.builtin.trs.clickhouse import ClickhouseStrTypeReprSystem, ChType
from coral.core.coral import CoralTypeReprSystem


def transform_coral_to_pa(from_: CoralType) -> PaType:
    assert from_ in PyArrowTypeReprSystem.nodes

    to_type_repr = [pred for pred in PyArrowTypeReprSystem.predecessors(from_) if isinstance(pred, PaType)].pop()
    return to_type_repr


def transform_ch_to_pa(from_: ChType) -> PaType:
    nx.compose_all(
        [
            PyArrowTypeReprSystem,
            ClickhouseStrTypeReprSystem,
            CoralTypeReprSystem
        ]
    )

    _coral_type = from_.semantic_type

    _pa_type_repr = list(PyArrowTypeReprSystem.predecessors(_coral_type)).pop()
    return _pa_type_repr


def transform_cr_to_pa(from_: CoralType) -> PaType:
    _pa_ctro = list(PyArrowTypeReprSystem.predecessors(from_)).pop()
    return _pa_ctro


def transform_cr_to_ch(from_: CoralType, cast: bool = False) -> ChType:
    def recursively_transform_parameters(params: MorphParams) -> dict:
        """
        # recursively transform param types if STRO is higher level

        :param params:
        :return:
        """
        # dict(pydantic.BaseModel) will not recursively unpack submodel,
        #  while pydantic.BaseModel.dict() will.
        #  in cases where some fields of MorphParams is another TRO, we need to keep it as is
        #  ref: https://docs.pydantic.dev/usage/exporting_models/
        params_dict = dict(params)

        for key, val in params_dict.items():
            if isinstance(val, CoralType):
                trans_param_value = transform_cr_to_ch(val)
                params_dict.update({key: trans_param_value})
        return params_dict

    ctro: ChType

    # 1. 直接对应
    if from_ in ClickhouseStrTypeReprSystem.nodes:
            ctro = list(ClickhouseStrTypeReprSystem.predecessors(from_)).pop()
            if cast:
                return ctro.cast()
            return ctro

    # 2. ancestor直接对应
    assert from_.ancestor in CoralTypeReprSystem.nodes, "The Higher Level STRO is not in the tree, please check."
    G = CoralTypeReprSystem.merge_with(ClickhouseStrTypeReprSystem)
    stro_ancestors = [n for n in G.predecessors(from_.ancestor) if isinstance(n, ChType)]
    if len(stro_ancestors) > 1:
        raise ValueError("This should not happen.")
    if len(stro_ancestors) == 1:
        stro_ancestor = stro_ancestors.pop()
        params_dict = recursively_transform_parameters(from_.params)
        ctro = stro_ancestor(**params_dict)
        if cast:
            return ctro.cast()
        return ctro

    # 3. complementary对应

    stro_candidates = list(G.stro_subgraph.predecessors(from_.ancestor))
    # stro_candidates.append(from_.ancestor)

    complementary_check = [from_.params.complementary(n.params) for n in stro_candidates]

    if sum(complementary_check) > 1:
        raise ValueError("This should not happen.")
    if sum(complementary_check) == 1:
        i = complementary_check.index(True)
        stro = stro_candidates[i]
        ctro = [n for n in G.predecessors(stro) if isinstance(n, ChType)].pop()

        params_dict = recursively_transform_parameters(from_.params)
        ctro = ctro(**params_dict)
        if cast:
            return ctro.cast()
        return ctro

    raise ValueError("Did not find compatible type.")
