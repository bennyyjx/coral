from __future__ import annotations

import inspect
import logging
from contextlib import contextmanager
from inspect import signature

from typing import Optional, Callable, Any

from pydantic import BaseModel, Field, create_model
from griffe.docstrings.parsers import Parser, parse
from griffe.dataclasses import Docstring
from griffe.docstrings.dataclasses import DocstringSectionKind


@contextmanager
def disable_logger(name: str):
    """
    Get a logger by name and disables it within the context manager.
    Upon exiting the context manager, the logger is returned to its
    original state.
    """
    logger = logging.getLogger(name=name)

    # determine if it's already disabled
    base_state = logger.disabled
    try:
        # disable the logger
        logger.disabled = True
        yield
    finally:
        # return to base state
        logger.disabled = base_state


def parameter_docstrings(docstring: Optional[str]) -> dict[str, str]:
    """
    Given a docstring in Google docstring format, parse the parameter section
    and return a dictionary that maps parameter names to docstring.
    Args:
        docstring: The function's docstring.
    Returns:
        Mapping from parameter names to docstrings.
    """
    param_docstrings: dict[str, str] = {}

    if not docstring:
        return param_docstrings

    with disable_logger("griffe.docstrings.google"), disable_logger(
        "griffe.agents.nodes"
    ):
        parsed = parse(Docstring(docstring), Parser.google)
        for section in parsed:
            if section.kind != DocstringSectionKind.parameters:
                continue
            param_docstrings = {
                parameter.name: parameter.description for parameter in section.value
            }

    return param_docstrings


def assert_mutually_exclusive(a: Any, b: Any):
    # TODO: refine implementation, distinguish when 'None' is a valid value
    false_predicate_1 = (a is None and b is None)
    false_predicate_2 = (a is not None and b is not None)

    if false_predicate_1 or false_predicate_2:
        raise AssertionError("a and b is not mutually exclusive")


def create_model_from_signature(fn: Callable,
                                model_name: str = None,
                                model_name_template: Callable = None,
                                base_cls: type[BaseModel] = BaseModel):
    # ref: https://github.com/PrefectHQ/prefect/blob/91214887785d8d17447d7b771ec7e4391dc42e5a/src/prefect/utilities/callables.py#L251
    assert_mutually_exclusive(model_name, model_name_template)

    sig = signature(fn)
    aliases = {}
    model_fields = {}
    doc_strings = parameter_docstrings(inspect.getdoc(fn))

    _model_name = model_name or model_name_template(fn)

    for position, param in enumerate(sig.parameters.values()):
        # Pydantic model creation will fail if names collide with the BaseModel type
        if hasattr(BaseModel, param.name):
            name = param.name + "__"
            aliases[name] = param.name
        else:
            name = param.name

        type_, field = (
            Any if param.annotation is sig.empty else param.annotation,
            Field(
                default=... if param.default is param.empty else param.default,
                title=param.name,
                description=doc_strings.get(param.name, None),
                alias=aliases.get(name),
                position=position,
            ),
        )

        # Generate a Pydantic model at each step so we can check if this parameter
        # type is supported schema generation
        try:
            create_model(
                "CheckParameter", **{name: (type_, field)}
            ).schema(by_alias=True)
        except ValueError:
            # This field's type is not valid for schema creation, update it to `Any`
            type_ = Any

        model_fields[name] = (type_, field)

    model = create_model(_model_name,
                         __base__=base_cls,
                         **model_fields)

    return model
