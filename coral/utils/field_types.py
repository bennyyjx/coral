from typing import ClassVar, TypeAlias

from pydantic import BaseModel


class SupportedBitLen(int):
    choices: ClassVar[tuple[int]]

    def __init_subclass__(cls, choices, **kwargs):
        super().__init_subclass__(**kwargs)
        cls.choices = choices

    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if v not in cls.choices:
            raise ValueError("The parameter specified ({bit_len:=}) is not supported.")
        return v

    def __repr__(self):
        return f'SupportedBitLen({super().__repr__()})'


class SupportedIntBitLen(SupportedBitLen, choices=(8, 16, 32, 64, 128, 256)):
    """..."""


class SupportedFloatBitLen(SupportedBitLen, choices=(32, 64)):
    """..."""


TimeZone: TypeAlias = str


class SupportedTimestampUnit(str):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not isinstance(v, str):
            raise TypeError
        if v not in {"ms", "us", "ns"}:
            raise TypeError

    def __repr__(self):
        return f'SupportedTimestampUnit({super().__repr__()})'
