from __future__ import annotations

from coral.core.base import TypeReprSystem, CR

CR_CH = TypeReprSystem(name="Coral ClickHouse", description="Coral's builtin representation (as string) of ClickHouse type system.")
PA = TypeReprSystem(name="PyArrow")
PR = TypeReprSystem(name="Pandera")
CC = TypeReprSystem(name="clickhouse-connect")
DG = TypeReprSystem(name="Dagster")
SA = TypeReprSystem(name="SQLAlchemy")
IB = TypeReprSystem(name="Ibis")

TypeReprSystems = {
    "CR": CR,
    "CR_CH": CR_CH,
    "PA": PA,
    "PR": PR,
    "CC": CC,
    "DG": DG,
    "SA": SA,
    "IB": IB,
}
