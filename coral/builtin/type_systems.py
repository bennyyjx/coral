from __future__ import annotations

from coral.core.base import TypeSystem, TypeSystemKind

PG = TypeSystem(name='PostgreSQL', kind=TypeSystemKind.DB)
CH = TypeSystem(name='ClickHouse', kind=TypeSystemKind.DB)
AR = TypeSystem(name='Arrow', kind=TypeSystemKind.XL, description="Arrow Columnar Format")
PL = TypeSystem(name='Polars', kind=TypeSystemKind.XL)
PD = TypeSystem(name='Pandas', kind=TypeSystemKind.PY)
NP = TypeSystem(name='NumPy', kind=TypeSystemKind.PY)
DC = TypeSystem(name='DaCe', kind=TypeSystemKind.PY)


TypeSystems = {
    "PG": PG,
    "CH": CH,
    "AR": AR,
    "PL": PL,
    "PD": PD,
    "NP": NP,
}
