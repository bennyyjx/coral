import coral.builtin.type_systems as ts
import coral.builtin.type_repr_systems as trs
import coral.core.base

# TODO: typing the structure
repr_implementations = (
    (coral.core.base.CR, ts.CH),
    (coral.core.base.CR, ts.PG),
    (coral.core.base.CR, ts.AR),
    (coral.core.base.CR, ts.PL),

    (trs.PA, ts.AR),

    (trs.CC, ts.CH),
)

repr_mappings = (
    (coral.core.base.CR, trs.PA),
    (coral.core.base.CR, trs.CR_CH),

)
