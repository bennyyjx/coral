import networkx as nx

import coral.builtin.type_repr_systems as repr_systems
import pyarrow as pa

import coral.core.coral as coral
from coral.core.hub import UnconnectedNodes, TRS
from coral.core.repr import ConcreteTypeReprObject
from coral.utils.field_types import SupportedTimestampUnit


class PaType(ConcreteTypeReprObject, repr_system=repr_systems.PA):
    """PyArrow's representation of Arrow type system."""


PaType.update_forward_refs()


@coral.CoralBool.deco(tro_cls=PaType)
def PaBool():
    return pa.bool_()


@coral.CoralInt.deco(tro_cls=PaType)
def PaInt(bit_len: int, signed: bool):
    match signed, bit_len:
        case (True, 8):
            return pa.int8()
        case (True, 16):
            return pa.int16()
        case (True, 32):
            return pa.int32()
        case (True, 64):
            return pa.int64()
        case (False, 8):
            return pa.uint8()
        case (False, 16):
            return pa.uint16()
        case (False, 32):
            return pa.uint32()
        case (False, 64):
            return pa.uint64()
        case _:
            raise ValueError


@coral.CoralFloatNumber.deco(tro_cls=PaType)
def PaFloat(bit_len: int):
    if bit_len == 16:
        return pa.float16()
    elif bit_len == 32:
        return pa.float32()
    elif bit_len == 64:
        return pa.float64()
    else:
        raise ValueError


@coral.CoralFixedNumber.deco(tro_cls=PaType)
def PaDecimal(precision: int, scale: int):
    morph_decimal = pa.decimal128 if precision <= 38 else pa.decimal256
    return morph_decimal(int_precision=precision, int_scale=scale)


@coral.CoralVarString.deco(tro_cls=PaType)
def PaString(is_large: bool):
    if is_large:
        return pa.large_string()
    else:
        return pa.string()


@coral.CoralTimestamp.deco(tro_cls=PaType)
def PaTimestamp(unit: SupportedTimestampUnit, tz: str):
    kwargs = {"unit": unit}
    if tz:
        kwargs.update({"tz": tz})
    return pa.timestamp(**kwargs)


@coral.CoralDictionary.deco(tro_cls=PaType)
def PaDictionary(index_type: PaType, value_type: PaType, ordered: bool):
    assert index_type.params.fields_init_status == "full"
    assert value_type.params.fields_init_status == "full"

    return pa.dictionary(index_type(), value_type(), ordered)


@coral.CoralVarBinary.deco(tro_cls=PaType)
def PaVarBinary():
    return pa.binary(int_length=-1)


@coral.CoralFixedBinary.deco(tro_cls=PaType)
def PaFixedBinary(byte_len: int):
    return pa.binary(int_length=byte_len)


PaUInt8 = PaInt(bit_len=8, signed=False)
PaUInt16 = PaInt(bit_len=16, signed=False)
PaUInt32 = PaInt(bit_len=32, signed=False)
PaUInt64 = PaInt(bit_len=64, signed=False)
PaInt8 = PaInt(bit_len=8, signed=True)
PaInt16 = PaInt(bit_len=16, signed=True)
PaInt32 = PaInt(bit_len=32, signed=True)
PaInt64 = PaInt(bit_len=64, signed=True)

PaFloat32 = PaFloat(bit_len=32)
PaFloat64 = PaFloat(bit_len=64)

PaRegularString = PaString(is_large=False)
PaLargeString = PaString(is_large=True)

PaDatetime64_ms_08 = PaTimestamp(unit='ms', tz='Asia/Shanghai')


PyArrowTypeReprSystem = TRS.build(
    [
        PaBool,
        PaInt,
        PaFloat,
        PaDecimal,
        PaString,
        PaTimestamp,
        PaDictionary,
        PaUInt8,
        PaUInt16,
        PaUInt32,
        PaUInt64,
        PaInt8,
        PaInt16,
        PaInt32,
        PaInt64,
        PaFloat32,
        PaFloat64,
        PaRegularString,
        PaLargeString,
        PaDatetime64_ms_08,
    ]
)
