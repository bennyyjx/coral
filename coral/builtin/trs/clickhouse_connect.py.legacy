from coral.builtin.semantic_types import SemanticInteger, SemanticRealNumber, SemanticTemporal, SemanticText, \
    SemanticChoices, SemanticBinary
from coral.builtin.type_params import IntegerTypeParams, FixedPointNumberTypeParams, \
    FloatingPointNumberTypeParams, DatetimeTypeParams, FixedLengthStringTypeParams, EnumTypeParams, BinaryTypeParams
from coral.core.repr import TypeReprObject, TOBJ2
import coral.builtin.type_repr_systems as repr_systems


class ChCcType(TypeReprObject, repr_system=repr_systems.CC):
    """Representation of ClickHouse Data Types System in clickhouse_connect."""


ChCcInt = ChCcType(level=TOBJ2,
                   semantic_type=SemanticInteger,
                   morph_args=IntegerTypeParams)

ChCcFloat = ChCcType(level=TOBJ2,
                     semantic_type=SemanticRealNumber,
                     morph_args=FloatingPointNumberTypeParams)

ChCcDecimal = ChCcType(level=TOBJ2,
                       semantic_type=SemanticRealNumber,
                       morph_args=FixedPointNumberTypeParams)

ChCcDateTime = ChCcType(level=TOBJ2,
                        semantic_type=SemanticTemporal,
                        morph_args=DatetimeTypeParams)

ChCcString = ChCcType(level=TOBJ2,
                      semantic_type=SemanticText,
                      morph_args=FixedLengthStringTypeParams)

ChCcUInt8 = ChCcInt(bit_len=8, signness=False)
ChCcUInt16 = ChCcInt(bit_len=16, signness=False)
ChCcUInt32 = ChCcInt(bit_len=32, signness=False)
ChCcUInt64 = ChCcInt(bit_len=64, signness=False)
ChCcInt8 = ChCcInt(bit_len=8, signness=True)
ChCcInt16 = ChCcInt(bit_len=16, signness=True)
ChCcInt32 = ChCcInt(bit_len=32, signness=True)
ChCcInt64 = ChCcInt(bit_len=64, signness=True)

ChCcFloat32 = ChCcFloat(bit_len=32)
ChCcFloat64 = ChCcFloat(bit_len=64)

ChCcText = ChCcString()

ChCcDatetime64_ms_08 = ChCcDateTime(unit='ms', timezone='Asia/Shanghai')
ChCcEnum = ChCcType(level=TOBJ2,
                    semantic_type=SemanticChoices,
                    morph_args=EnumTypeParams)

ChCcLowCardinality = ChCcEnum(is_explicit=False)

ChCcBin = ChCcType(level=TOBJ2,
                   semantic_type=SemanticBinary,
                   morph_args=BinaryTypeParams)

ChCcBinary = ChCcBin()
