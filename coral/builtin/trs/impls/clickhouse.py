from __future__ import annotations
from pydantic import validator, root_validator
from typing_extensions import Self

from coral import PKG_ROOT_DIR
from coral.core.base import StringImpl

ChCanonicalDataPath = PKG_ROOT_DIR / 'builtin/cc.csv'


class ChStringImpl(StringImpl, canonical_data_path=ChCanonicalDataPath):
    case_sensitive: bool
    is_canonical: bool = False
    alternatives: tuple[ChStringImpl, ...] | None = None

    @classmethod
    def _value_in_canonical_data(cls, v, col_index: int) -> bool:
        if col_index >= (max_idx := len(cls.canonical_data[0])):
            raise ValueError(f"The provided {col_index :=} is out of range, {max_idx =}")
        for row in cls.canonical_data:
            if v == row[col_index]:
                return True
        return False

    @validator("repr")
    def check_value_against_canonical_data(cls, v):
        assert cls._value_in_canonical_data(v, 0)
        return v

    @validator("alternatives")
    def validate_alternatives(cls, alternatives: tuple[Self, ...], values):
        is_canonical: bool = values["is_canonical"]
        if is_canonical:
            for alt in alternatives:
                assert alt.repr != values['repr'], "duplicated alternative."
                assert alt.is_canonical is False
                assert alt.alternatives is None
        else:
            assert alternatives is None

        return alternatives

    def __get__(self, instance, owner=None):
        return self.repr

    def __call__(self) -> str:
        return self.repr


# TODO: Ideally the validation should be performed in the test suite, and here the `construct()` method should be used to skip validation & improve performance.


ChBoolStrRepr = ChStringImpl(repr="Bool",
                             case_sensitive=False,
                             is_canonical=True,
                             alternatives=(
                                 ChStringImpl(repr="bool", case_sensitive=False),
                                 ChStringImpl(repr="boolean", case_sensitive=False),
                             ))

ChInt8StrRepr = ChStringImpl(repr="Int8",
                             case_sensitive=True,
                             is_canonical=True,
                             alternatives=(
                                 ChStringImpl(repr="TINYINT SIGNED", case_sensitive=False),
                                 ChStringImpl(repr="INT1 SIGNED", case_sensitive=False),
                                 ChStringImpl(repr="INT1", case_sensitive=False),
                                 ChStringImpl(repr="TINYINT", case_sensitive=False),
                                 ChStringImpl(repr="BYTE", case_sensitive=False),
                             ))

ChInt16StrRepr = ChStringImpl(repr="Int16",
                              case_sensitive=True,
                              is_canonical=True,
                              alternatives=(
                                  ChStringImpl(repr="SMALLINT", case_sensitive=False),
                                  ChStringImpl(repr="SMALLINT SIGNED", case_sensitive=False),))

ChInt32StrRepr = ChStringImpl(repr="Int32",
                              case_sensitive=True,
                              is_canonical=True,
                              alternatives=(
                                  ChStringImpl(repr="INTEGER SIGNED", case_sensitive=False),
                                  ChStringImpl(repr="INT SIGNED", case_sensitive=False),
                                  ChStringImpl(repr="MEDIUMINT", case_sensitive=False),
                                  ChStringImpl(repr="INTEGER", case_sensitive=False),
                                  ChStringImpl(repr="INT", case_sensitive=False),
                                  ChStringImpl(repr="MEDIUMINT SIGNED", case_sensitive=False),
                              ))

ChInt64StrRepr = ChStringImpl(repr="Int64",
                              case_sensitive=True,
                              is_canonical=True,
                              alternatives=(
                                  ChStringImpl(repr="INTEGER SIGNED", case_sensitive=False),
                                  ChStringImpl(repr="INT SIGNED", case_sensitive=False),
                                  ChStringImpl(repr="MEDIUMINT", case_sensitive=False),
                              ))

ChUInt8StrRepr = ChStringImpl(repr="UInt8",
                              case_sensitive=True,
                              is_canonical=True,
                              alternatives=(
                                  ChStringImpl(repr="INT1 UNSIGNED", case_sensitive=False),
                                  ChStringImpl(repr="TINYINT UNSIGNED", case_sensitive=False),
                              ))

ChUInt16StrRepr = ChStringImpl(repr="UInt16",
                               case_sensitive=True,
                               is_canonical=True,
                               alternatives=(
                                   ChStringImpl(repr="SMALLINT UNSIGNED", case_sensitive=False),
                                   ChStringImpl(repr="YEAR", case_sensitive=False),
                               ))

ChUInt32StrRepr = ChStringImpl(repr="UInt32",
                               case_sensitive=True,
                               is_canonical=True,
                               alternatives=(
                                   ChStringImpl(repr="INTEGER UNSIGNED", case_sensitive=False),
                                   ChStringImpl(repr="INT UNSIGNED", case_sensitive=False),
                                   ChStringImpl(repr="MEDIUMINT UNSIGNED", case_sensitive=False),
                               ))

ChUInt64StrRepr = ChStringImpl(repr="UInt64",
                               case_sensitive=True,
                               is_canonical=True,
                               alternatives=(
                                   ChStringImpl(repr="SET", case_sensitive=False),
                                   ChStringImpl(repr="BIT", case_sensitive=False),
                                   ChStringImpl(repr="BIGINT UNSIGNED", case_sensitive=False),
                               ))

ChFloat32StrRepr = ChStringImpl(repr="Float32",
                                case_sensitive=True,
                                is_canonical=True,
                                alternatives=(
                                    ChStringImpl(repr="FLOAT", case_sensitive=False),
                                    ChStringImpl(repr="REAL", case_sensitive=False),
                                    ChStringImpl(repr="SINGLE", case_sensitive=False),
                                ))

ChFloat64StrRepr = ChStringImpl(repr="Float64",
                                case_sensitive=True,
                                is_canonical=True,
                                alternatives=(
                                    ChStringImpl(repr="DOUBLE PRECISION", case_sensitive=False),
                                    ChStringImpl(repr="DOUBLE", case_sensitive=False),
                                ))

ChDecimalStrRepr = ChStringImpl(repr="Decimal",
                                case_sensitive=False,
                                is_canonical=True,
                                alternatives=(
                                    ChStringImpl(repr="FIXED", case_sensitive=False),
                                    ChStringImpl(repr="NUMERIC", case_sensitive=False),
                                    ChStringImpl(repr="DEC", case_sensitive=False),
                                ))

ChStringStrRepr = ChStringImpl(repr="String",
                               case_sensitive=True,
                               is_canonical=True,
                               alternatives=(
                                   ChStringImpl(repr="GEOMETRY", case_sensitive=False),
                                   ChStringImpl(repr="NATIONAL CHAR VARYING", case_sensitive=False),
                                   ChStringImpl(repr="BINARY VARYING", case_sensitive=False),
                                   ChStringImpl(repr="NCHAR LARGE OBJECT", case_sensitive=False),
                                   ChStringImpl(repr="NATIONAL CHARACTER VARYING", case_sensitive=False),
                                   ChStringImpl(repr="NATIONAL CHARACTER LARGE OBJECT", case_sensitive=False),
                                   ChStringImpl(repr="NATIONAL CHARACTER", case_sensitive=False),
                                   ChStringImpl(repr="NATIONAL CHAR", case_sensitive=False),
                                   ChStringImpl(repr="CHARACTER VARYING", case_sensitive=False),
                                   ChStringImpl(repr="LONGBLOB", case_sensitive=False),
                                   ChStringImpl(repr="TINYBLOB", case_sensitive=False),
                                   ChStringImpl(repr="MEDIUMTEXT", case_sensitive=False),
                                   ChStringImpl(repr="TEXT", case_sensitive=False),
                                   ChStringImpl(repr="VARCHAR2", case_sensitive=False),
                                   ChStringImpl(repr="CHARACTER LARGE OBJECT", case_sensitive=False),
                                   ChStringImpl(repr="LONGTEXT", case_sensitive=False),
                                   ChStringImpl(repr="NVARCHAR", case_sensitive=False),
                                   ChStringImpl(repr="VARCHAR", case_sensitive=False),
                                   ChStringImpl(repr="CHAR VARYING", case_sensitive=False),
                                   ChStringImpl(repr="MEDIUMBLOB", case_sensitive=False),
                                   ChStringImpl(repr="NCHAR", case_sensitive=False),
                                   ChStringImpl(repr="VARBINARY", case_sensitive=False),
                                   ChStringImpl(repr="CHAR", case_sensitive=False),
                                   ChStringImpl(repr="TINYTEXT", case_sensitive=False),
                                   ChStringImpl(repr="CLOB", case_sensitive=False),
                                   ChStringImpl(repr="BLOB", case_sensitive=False),
                                   ChStringImpl(repr="NCHAR VARYING", case_sensitive=False),
                                   ChStringImpl(repr="BINARY LARGE OBJECT", case_sensitive=False),
                                   ChStringImpl(repr="BYTEA", case_sensitive=False),
                                   ChStringImpl(repr="CHAR LARGE OBJECT", case_sensitive=False),
                                   ChStringImpl(repr="CHARACTER", case_sensitive=False),
                               ))

ChFixedStringStrRepr = ChStringImpl(repr="FixedString",
                                    case_sensitive=True,
                                    is_canonical=True,
                                    alternatives=(
                                        ChStringImpl(repr="BINARY", case_sensitive=False),
                                    ))

ChDate32StrRepr = ChStringImpl(repr="Date32",
                               case_sensitive=True,
                               is_canonical=True)

ChDateTimeStrRepr = ChStringImpl(repr="DateTime",
                                 case_sensitive=True,
                                 is_canonical=True,
                                 alternatives=(
                                     ChStringImpl(repr="TIMESTAMP", case_sensitive=False),
                                 ))

ChDateTime64StrRepr = ChStringImpl(repr="DateTime64",
                                   case_sensitive=True,
                                   is_canonical=True)

ChEnumStrRepr = ChStringImpl(repr="Enum",
                             case_sensitive=True,
                             is_canonical=True,
                             alternatives=(
                                 ChStringImpl(repr="ENUM", case_sensitive=False),
                             ))

ChLowCardStrRepr = ChStringImpl(repr="LowCardinality",
                                case_sensitive=True,
                                is_canonical=True)
