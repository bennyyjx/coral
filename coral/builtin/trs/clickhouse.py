import networkx as nx

from coral.core.hub import TRS
from coral.core.repr import ConcreteTypeReprObject
import coral.builtin.type_repr_systems as repr_systems
import coral.core.coral as coral

import coral.builtin.trs.impls.clickhouse as ch


class ChType(ConcreteTypeReprObject, repr_system=repr_systems.CR_CH):
    """Representation of ClickHouse Data Types System in string."""


@coral.CoralBool.deco(tro_cls=ChType)
def ChBool():
    return ch.ChBoolStrRepr


"""
@Morphism(_link_to=coral_int)
def ch_int(bit_len: int, signed: bool):
    if signed:
        if bit_len == 8:
            return ...
        elif ...
        ...
    else:
        ...
"""
# TODO: the Morphism decorator checks ch_int's signature with coral_int,
#   but how would missing parameters be filled?
#   looks like ch_int has to exactly conform to coral_int's signature, with the unneeded
#   parameters filled with a default value. see  below for example:

"""
def coral_dictionary(idx_type: CoralType = CoralInt64, value_type: CoralType, sorted: bool):
    ...

def ch_dictionary(value_type: ChType, idx_type = None, sorted = None):
    "the default value None indicates the parameters are not needed for this specific trs."
    ...
"""


@coral.CoralInt.deco(tro_cls=ChType)
def ChInt(bit_len: int, signed: bool):
    if signed:
        if bit_len == 8:
            return ch.ChInt8StrRepr()
        elif bit_len == 16:
            return ch.ChInt16StrRepr()
        elif bit_len == 32:
            return ch.ChInt32StrRepr()
        elif bit_len == 64:
            return ch.ChInt64StrRepr()
        else:
            raise ValueError
    else:
        if bit_len == 8:
            return ch.ChUInt8StrRepr()
        elif bit_len == 16:
            return ch.ChUInt16StrRepr()
        elif bit_len == 32:
            return ch.ChUInt32StrRepr()
        elif bit_len == 64:
            return ch.ChUInt64StrRepr()
        else:
            raise ValueError


@coral.CoralFloatNumber.deco(tro_cls=ChType)
def ChFloat(bit_len: int):
    if bit_len == 32:
        return ch.ChFloat32StrRepr()
    elif bit_len == 64:
        return ch.ChFloat64StrRepr()
    else:
        raise ValueError


@coral.CoralFixedNumber.deco(tro_cls=ChType)
def ChDecimal(precision: int, scale: int):
    # validation on input parameters should be unnecessary since the parameters are provided by some CoralType.params which has already been validated.
    # assert precision > scale
    repr = f"{ch.ChDecimalStrRepr()}({precision}, {scale})"
    return repr


@coral.CoralFixedByteString.deco(tro_cls=ChType)
def ChFixedString(byte_len: int):
    repr = f"{ch.ChFixedStringStrRepr()}({byte_len})"
    return repr


@coral.CoralVarString.deco(tro_cls=ChType, is_large=False)
def ChString():
    return ch.ChStringStrRepr()


@coral.CoralDate.deco(tro_cls=ChType)
def ChDate32():
    return ch.ChDate32StrRepr()


@coral.CoralTimestamp.deco(tro_cls=ChType)
def ChDateTime64(unit, tz):
    # TODO: the _mapping is a hack
    _mapping = {
        "s": 0,
        "ms": 3,
        "us": 6,
        "ns": 9,
    }
    repr = f"{ch.ChDateTime64StrRepr()}({_mapping[unit]}, '{tz}')"
    return repr


# TODO: ideally: value_type: TypeReprObject[ClickHouseTypeReprSystem]
@coral.CoralDictionary.deco(tro_cls=ChType, index_type=ChInt(bit_len=32, signed=True), ordered=False)
def ChLowCardinality(value_type: ChType = ChString):
    repr = f"{ch.ChLowCardStrRepr()}({value_type()})"
    return repr


ChUInt8 = ChInt(bit_len=8, signed=False)
ChUInt16 = ChInt(bit_len=16, signed=False)
ChUInt32 = ChInt(bit_len=32, signed=False)
ChUInt64 = ChInt(bit_len=64, signed=False)
ChInt8 = ChInt(bit_len=8, signed=True)
ChInt16 = ChInt(bit_len=16, signed=True)
ChInt32 = ChInt(bit_len=32, signed=True)
ChInt64 = ChInt(bit_len=64, signed=True)

ChFloat32 = ChFloat(bit_len=32)
ChFloat64 = ChFloat(bit_len=64)

ChDatetime64_ms_08 = ChDateTime64(unit='ms', tz='Asia/Shanghai')

ClickhouseStrTypeReprSystem = TRS[ChType].build(
    [
        ChBool,
        ChInt,
        ChFloat,
        ChDecimal,
        ChString,
        ChFixedString,
        ChDate32,
        ChDateTime64,
        ChLowCardinality,
        ChUInt8,
        ChUInt16,
        ChUInt32,
        ChUInt64,
        ChInt8,
        ChInt16,
        ChInt32,
        ChInt64,
        ChFloat32,
        ChFloat64,
        ChDatetime64_ms_08
    ]
)
