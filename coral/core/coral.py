from __future__ import annotations

from typing import Literal

import networkx as nx
from pydantic import validator, Field, PositiveInt

from coral.core.base import CoralBaseModel, CR
from coral.core.hub import TRS
from coral.core.repr import SemanticTypeReprObject, MorphParams, TypeReprObject
from coral.utils.field_types import SupportedIntBitLen, SupportedFloatBitLen, TimeZone


class CoralType(SemanticTypeReprObject, repr_system=CR):  # formerly CoralType
    """
    作为Decorator，不接受除underlying之外的额外参数
    """
    # TODO: improve decorator factory typing
    #   ref: https://mypy.readthedocs.io/en/stable/generics.html?highlight=decorator#decorator-factories

@CoralType
class CoralNumber(MorphParams):
    """..."""


@CoralType
class CoralInt(MorphParams):
    # TODO: the reason that the model fields has default value of "None" is to allow
    #   partial application / incremental building / ..., essentially this class
    bit_len: int = None  # TODO: refine typing - SupportedIntBitLen
    signed: bool = None


@CoralType
class CoralRealNumber(MorphParams):
    ...


@CoralType
class CoralFloatNumber(MorphParams):
    bit_len: int = None  # TODO: refine typing - SupportedFloatBitLen


@CoralType
class CoralFixedNumber(MorphParams):
    precision: int = None
    scale: int = None

    @validator("precision")
    def validate_precision(cls, v: int):
        assert v > 1
        return v

    @validator("scale")
    def validate_scale(cls, v: int, values: dict):
        precision = values["precision"]
        assert 0 <= v < precision
        return v


@CoralType
class CoralText(MorphParams):
    ...


@CoralType
class CoralVarString(MorphParams):
    is_large: bool = Field(None, description="Arrow specific.")


@CoralType
class CoralVarcharString(MorphParams):
    size: PositiveInt = Field(None, description="Storing up to `size` characters.")


@CoralType
class CoralFixedByteString(MorphParams):
    byte_len: PositiveInt = Field(None, description="Storing Exactly `size` bytes, Clickhouse specific.")


@CoralType
class CoralTemporal(MorphParams):
    """..."""


@CoralType
class CoralDate(MorphParams):
    """..."""


@CoralType
class CoralTime(MorphParams):
    """..."""


@CoralType
class CoralDuration(MorphParams):
    """..."""


@CoralType
class CoralTimestamp(MorphParams):
    """..."""
    unit: Literal['s', 'ms', 'us', 'ns'] = None
    tz: TimeZone = None


@CoralType
class CoralChoices(MorphParams):
    """..."""


@CoralType
class CoralBool(MorphParams):
    """..."""


@CoralType
class CoralDictionary(MorphParams):
    """Dictionary type, following Arrow's definition."""
    index_type: TypeReprObject = None
    value_type: TypeReprObject = None
    ordered: bool = None

    @validator("index_type")
    def index_type_should_be_fully_parametrized(cls, v):
        v: TypeReprObject
        assert v.params.fields_init_status == "full", "index_type should be level1 TypeReprObject."
        return v


@CoralType
class CoralEnum(MorphParams):
    """..."""
    variants: tuple = None


@CoralType
class CoralBinaryData(MorphParams):
    """..."""


@CoralType
class CoralVarBinary(MorphParams):
    """Variable length binary data."""


@CoralType
class CoralMimeBinary(MorphParams):
    """..."""
    # TODO: should subclass CoralVarBinary?
    #   currently this is not possible, as subclassing CoralVarBinary will result in subclassing from STRO rather than MorphParams, need fix
    mime_type: str = None  # TODO: custom field type


@CoralType
class CoralFixedBinary(MorphParams):
    """Fixed length binary data."""
    byte_len: int = None


### Level 1 Types ###

CoralUnsignedInt = CoralInt(signed=False)
CoralSignedInt = CoralInt(signed=True)

CoralUInt8 = CoralUnsignedInt(bit_len=8)

### Coral Type System ###

coral_type_system_edges = [
    (CoralSignedInt, CoralInt),
    (CoralUnsignedInt, CoralInt),
    (CoralFloatNumber, CoralRealNumber),
    (CoralFixedNumber, CoralRealNumber),
    (CoralInt, CoralNumber),
    (CoralRealNumber, CoralNumber),

    (CoralVarString, CoralText),
    (CoralFixedByteString, CoralText),
    (CoralVarcharString, CoralText),

    (CoralTimestamp, CoralTemporal),
    (CoralDate, CoralTemporal),
    (CoralTime, CoralTemporal),
    (CoralDuration, CoralTemporal),

    (CoralBool, CoralChoices),
    (CoralDictionary, CoralChoices),
    (CoralEnum, CoralChoices),

    (CoralVarBinary, CoralBinaryData),
    (CoralFixedBinary, CoralBinaryData),
]

CoralTypeReprSystem = TRS()
CoralTypeReprSystem.add_edges_from(coral_type_system_edges)
