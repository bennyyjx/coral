from __future__ import annotations

import itertools
from typing import Generic, TypeVar, Collection

import networkx as nx
from pydantic import root_validator
from pydantic.fields import ModelField
from pydantic.generics import GenericModel
from typing_extensions import Self

from coral.core.repr import TypeReprObject, SemanticTypeReprObject, ConcreteTypeReprObject

NodeT = TypeVar("NodeT", bound=TypeReprObject)
StroNodeT = TypeVar("StroNodeT", bound=SemanticTypeReprObject)
CtroNodeT = TypeVar("CtroNodeT", bound=ConcreteTypeReprObject)
LT = TypeVar("LT")
RT = TypeVar("RT")
PivotT = TypeVar("PivotT")


class TRS(nx.DiGraph, Generic[NodeT]):
    @classmethod
    def build(cls, nodes: Collection[NodeT]):
        node_semantic_type_map = ((node, node.semantic_type) for node in nodes)

        self = cls()
        self.add_nodes_from(nodes)
        self.add_edges_from(node_semantic_type_map)
        return self

    def _is_in(self, n: NodeT) -> bool:
        """Loosen the criteria to include TROs whose parent (level TOBJ2/3) are in the graph"""
        if n in self.nodes:
            return True
        if n.parent in self.nodes:  # TODO
            return True
        return False

    def merge_with(self, graph: TRS) -> Self:
        G: Self = nx.compose(self, graph)
        for node in G.nodes:
            if isinstance(node, SemanticTypeReprObject):
                G.add_edge(node, node.ancestor)
        return G

    @property
    def stro_subgraph(self) -> Self:
        nodes = (n for n in self.nodes if isinstance(n, SemanticTypeReprObject))
        return self.subgraph(nodes)

    @property
    def ctro_subgraph(self) -> Self:
        nodes = set(self.nodes) - set(self.stro_subgraph.nodes)
        return self.subgraph(nodes)

class MapperGraph(nx.DiGraph, Generic[LT, RT]):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v, field: ModelField):
        """
        Validation rules:
        1. the value should be an instance of networkx Graph
        2. nodes within `value` should be an instance of either LT or RT

        :param v:
        :param field:
        :return:
        """
        if not isinstance(v, nx.DiGraph):
            raise TypeError  # TODO: full error msg
        if not field.sub_fields:
            return v

        lt = field.sub_fields[0].outer_type_
        rt = field.sub_fields[1].outer_type_
        for node in v.nodes:
            if not (isinstance(node, lt) or isinstance(node, rt)):
                raise ValueError  # TODO: full error msg
        return v


class UnconnectedNodes(nx.Graph, Generic[NodeT]):
    """
    Pydantic field type for a networkx.Graph, with the following constraints:
    1. all nodes within the graph are instances of NodeT
    2. there is no edge between any of the nodes
    """

    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v, field: ModelField):
        if not isinstance(v, nx.Graph):
            raise TypeError  # TODO: full error msg
        if not field.sub_fields:
            # Generic parameters were not provided so we don't try to validate
            # them and just return the value as is
            return v
        nodet = field.sub_fields[0]
        for node in v.nodes:
            if not isinstance(node, nodet):
                raise TypeError
        if len(v.edges) > 0:
            raise ValueError

        return v


MapperT = TypeVar("MapperT", bound='DirectMapper')


class DirectMapper(GenericModel, Generic[LT, RT]):
    mapping: MapperGraph[LT, RT]
    _lefts: UnconnectedNodes[LT]
    _rights: UnconnectedNodes[RT]

    class Config:
        underscore_attrs_are_private = True
        validate_assignment = True

    @classmethod
    def build(cls: type[MapperT],
              lefts: Collection[LT],
              rights: Collection[RT],
              links: Collection[tuple] | None = None) -> MapperT[LT, RT]:
        """
        By intializing the Mapper instance this way, one implicitly ensures that `_lefts` & `_rights` satisfies the constraints of `UnconnectedNodes`.
        :param lefts:
        :param rights:
        :param links:
        :return:
        """
        links = () if links is None else links
        nodes = itertools.chain(lefts, rights)

        G = nx.DiGraph()
        G.add_nodes_from(nodes)
        G.add_edges_from(links)

        mapper = cls(mapping=G)
        mapper._lefts = G.subgraph(lefts)
        mapper._rights = G.subgraph(rights)

        return mapper

    def links(self, node: LT | RT) -> set[LT] | set[RT]:
        # TODO: overload to allow passing string as argument
        assert node in self.mapping
        return set(self.mapping.neighbors(node))

    @property
    def left_type(self):
        return type(self._lefts[0])

    @property
    def right_type(self):
        return type(self._rights[0])


class HomoDirectMapper(DirectMapper[LT, LT], Generic[LT]):
    @classmethod
    def build(cls,
              lefts: Collection[LT],
              rights: Collection[LT],
              links: Collection[tuple[LT, LT] | set[LT]] = None) -> HomoDirectMapper[LT]:
        """
        By intializing the Mapper instance this way, one implicitly ensures that `_lefts` & `_rights` satisfies the constraints of `UnconnectedNodes`.

        :param lefts:
        :param rights:
        :param links:
        :return:
        """

        set_links = itertools.chain.from_iterable(
            tuple(itertools.permutations(link, 2))
            for link in links if isinstance(link, set)
        )
        tuple_links = (
            link for link in links if isinstance(link, tuple)
        )
        all_links = tuple(itertools.chain(set_links, tuple_links))

        _self = super().build(lefts, rights, all_links)

        return _self


class PivotMapper(GenericModel, Generic[LT, RT, PivotT]):
    left: DirectMapper[PivotT, LT]
    right: DirectMapper[PivotT, RT]
    # pivot: MapperGraph[PivotT, PivotT]
    _bridge: MapperGraph

    @root_validator
    def build_bridge_mapper(cls, values):
        lg: DirectMapper[PivotT, LT] = values["left"]
        rg: DirectMapper[PivotT, RT] = values["right"]

        bridge_mapper = nx.compose(lg.mapping, rg.mapping)
