from __future__ import annotations

import warnings
from abc import ABC
from inspect import signature
from itertools import filterfalse
from typing import Literal, ClassVar, Callable, Any, TypeAlias, overload

from pydantic import BaseModel, Extra, root_validator, PrivateAttr, Field
from typing_extensions import Self

from coral.core.base import Attr, CoralBaseModel, TypeReprSystem

TypeConstructor: TypeAlias = Callable


class TypeLevel(Attr):
    """
    Alternative implementation of TypeLevel.
    """
    level: int
    description: str


TOBJ0 = TypeLevel(name="TOBJ0", level=0, description="value.")
TOBJ1 = TypeLevel(name="TOBJ1", level=1, description="type.")
TOBJ2 = TypeLevel(name="TOBJ2", level=2, description="level 1 type morphism.")
TOBJ3 = TypeLevel(name="TOBJ3", level=3, description="level 2 type morphism.")


class TypeReprLevels(BaseModel):
    TOBJ0: ClassVar[TypeLevel] = TOBJ0
    TOBJ1: ClassVar[TypeLevel] = TOBJ1
    TOBJ2: ClassVar[TypeLevel] = TOBJ2
    TOBJ3: ClassVar[TypeLevel] = TOBJ3


class MorphParams(CoralBaseModel):
    """
    Use this class's instances to generate a textual representation of TypedDict.
    The TypedDict class object can be used to annotate type class constructor's `kwargs` argument;
    This follows PEP692 with mypy support.
    To safely support such code generation behavior, additional qa tests should be placed in the test suite and run by CI / pre-commit hook.
    ref: https://github.com/python/mypy/issues/4441
    ref: https://peps.python.org/pep-0692/#reference-implementation
    """

    class Config:
        validate_assignment = True

    def __hash__(self):
        hashable_object = tuple(self.__dict__.items())
        return hash(hashable_object)

    @classmethod
    def compatible_with(cls, func: Callable) -> bool:
        from pydantic.decorator import (
            ValidatedFunction,
            ALT_V_ARGS,
            ALT_V_KWARGS,
            V_POSITIONAL_ONLY_NAME,
            V_DUPLICATE_KWARGS,
        )
        from pydantic import ConfigDict

        # ref: pydantic.decorator.ValidatedFunction.__init__
        excluded_field_names = {
            ALT_V_ARGS, ALT_V_KWARGS, V_POSITIONAL_ONLY_NAME, V_DUPLICATE_KWARGS, "args", "kwargs"
        }

        func_model: BaseModel = ValidatedFunction(func, config=ConfigDict(arbitrary_types_allowed=True)).model
        self_fields = {(field.name, field.type_) for field in cls.__fields__.values()}
        func_fields = {
            (field.name, field.type_) for field in func_model.__fields__.values()
            if field.name not in excluded_field_names
        }

        # TODO: properly implement compatibility check.
        #   currently only checks param names, due to additional complexities when the argument represents a type rather than a value
        #   e.g. PaDictionary(value_type: pa.DataType, ...) vs. PaInt(bit_len: int, signed: bool)
        #                    type __↑                             value __↑   value __↑
        # is_compatible = func_fields <= self_fields
        is_compatible = {f[0] for f in func_fields} <= {f[0] for f in self_fields}

        return is_compatible

    @property
    def fields_init_status(self) -> Literal['full', 'partial', 'none']:
        attrs_inited = [field is not None for field in self.__dict__.values()]

        if (not self.__fields__) or all(attrs_inited):
            return "full"
        if any(attrs_inited):
            return "partial"
        return "none"

    def complementary(self, other: Self) -> bool:
        from operator import xor
        def is_not_none(v) -> bool:
            res = False if v is None else True
            return res

        _comp_tbl = (
            (is_not_none(v_self), is_not_none(v_other))
            for v_self, v_other in zip(dict(self).values(), dict(other).values(), strict=True)
        )

        _comp_results = (xor(*pair) for pair in _comp_tbl)
        _comp_result = True if all(_comp_results) else False
        return _comp_result


class TypeReprObject(CoralBaseModel, ABC):
    repr_system: ClassVar[TypeReprSystem]
    underlying: Any  # TODO: type narrowing
    semantic_type: SemanticTypeReprObject | None
    params: MorphParams = Field(
        default=None,
        description="The parameters that construct the particular type. Could be partial."
    )
    default_params: MorphParams | None = Field(
        default=None,
        description="The default parameters used when casting a higher-level TRO to level 1 TRO. Should be the safest parameters combo possible."
    )

    class Config:
        underscore_attrs_are_private = True
        extra = Extra.forbid


class SemanticTypeReprObject(TypeReprObject, ABC):
    # name: str
    semantic_type: None = None  # semantic: redeclare to more precise typing
    underlying: type[MorphParams]
    default_params: None = Field(
        default=None,
        description="STRO does not have default_params as it cannot be casted."  # TODO: or can it?
    )

    _ctro_cls: type[ConcreteTypeReprObject] | None = PrivateAttr(default=None)
    _params: dict | None = PrivateAttr(default_factory=dict)

    @root_validator
    def init_params(cls, values: dict):
        params = values.get("params")
        if not params:
            default_factory: type[MorphParams] = values["underlying"]
            values.update(params=default_factory())
        return values

    def __init__(self, underlying, /, **kwargs):
        super().__init__(underlying=underlying, **kwargs)

    def __init_subclass__(cls, repr_system: TypeReprSystem, **kwargs):
        super().__init_subclass__(**kwargs)
        cls.repr_system = repr_system

    def __str__(self):
        return self.underlying.__name__ + str(hash(self))[-4:]

    def __repr__(self):
        return self.underlying.__name__ + str(hash(self))[-4:]

    def __hash__(self):
        distinguishing_fields = ("semantic_type", "underlying", "params",)
        hashable_object = tuple((k, v) for k, v in self.__dict__.items() if k in distinguishing_fields)
        return hash(hashable_object)

    @overload
    def __call__(self, morph_func: None = None, **kwargs) -> Self:
        ...

    @overload
    def __call__(self, morph_func: Callable, **kwargs) -> ConcreteTypeReprObject:
        ...

    def __call__(self, morph_func: Callable = None, **kwargs):
        #  MetaMorphism的underlying是dummy，不需要被call，因此__call__的唯二用途是：
        #   1. 在传入参数值的情况下完成自身的initialization, 此时的morph_func是coral_*, 返回一个MetaMorphism;
        #   2. 装饰别的callable，此时的morph_func是pa_*, ch_* etc.,返回一个Morphism
        #   例如：
        """
        @CoralInt(signed=True)
        def ch_int(bit_len: int):
            ...
        """

        match morph_func, kwargs:
            case None, {**extra} if extra:
                # SPECFIY-TYPE MODE
                params = self.params.copy(update=kwargs)
                new_stro = self.copy(update={"params": params})
                return new_stro

            case None, {**extra} if not extra:
                # called with no argument
                return self

            case func, {**extra} if extra:
                raise Exception(
                    "kwargs can only be provided when specifying types. It appears you are decorating a function.")
            case func, {**extra} if not extra:
                # DECORATOR MODE
                # Validation rules:
                #   1. signature(func).parameters compatible with self.underlying
                #   2. default_values.keys() and  signature(func).parameters.keys() are:
                #   2.1 disjoint, and
                #   2.2 the union of the two equals self.underlying
                assert self._ctro_cls is not None

                # TODO: validation rule 0 should be unnecessary
                # validation rule 0: func parameters cannot have default arguments
                # func_params_have_default_values = tuple(
                #     p.default is not p.empty
                #     for p in signature(func).parameters.values()
                # )
                # if any(func_params_have_default_values):
                #     raise AssertionError("func parameters cannot have default values")

                params = set(self._params)
                func_params = set(signature(func).parameters)
                defined_params = set(self.underlying.__fields__)

                match (
                    self.underlying.compatible_with(func),
                    params.isdisjoint(func_params),
                    (params | func_params) == defined_params
                ):
                    case True, True, True:
                        ctro_cls = self._ctro_cls
                        # transitively update self.semantic_type.params
                        params = self.underlying(**self._params)
                        semantic_type = self.copy(update={"params": params})
                        self._reset_tmp_fields()

                        return ctro_cls(func, semantic_type=semantic_type)
                    case _compatible, _is_disjoint, _union_equals:
                        # TODO: specify error messages based on matched bool values
                        raise AssertionError

    def deco(self, *, tro_cls: type[ConcreteTypeReprObject], **params):
        new_stro = self.copy()
        new_stro._ctro_cls = tro_cls
        new_stro._params = params
        return new_stro.__call__

    @property
    def name(self):
        return self.underlying.__name__

    def _reset_tmp_fields(self):
        self._ctro_cls = None
        self._params = None

    @property
    def ancestor(self) -> Self:  # TODO: rename this method
        if self.params.fields_init_status == "none":
            return self
        else:
            params = self.params.__class__()
            return self.copy(update={"params": params})


class ConcreteTypeReprObject(TypeReprObject, ABC):
    # semantic: self.semantic_type.underlying validates self.underlying
    semantic_type: SemanticTypeReprObject  # semantic: redeclare to more precise typing
    underlying: TypeConstructor  # TODO: narrow typing
    default_params: MorphParams = None

    @root_validator
    def init_params(cls, values: dict):
        params = values.get("params")
        if not params:
            semantic_type: SemanticTypeReprObject = values['semantic_type']
            default_factory: type[MorphParams] = semantic_type.underlying
            values.update(params=default_factory())
        return values

    def __init__(self, underlying, /, **params):
        super().__init__(underlying=underlying, **params)

    def __init_subclass__(cls, repr_system: TypeReprSystem, **kwargs):
        super().__init_subclass__(**kwargs)
        cls.repr_system = repr_system

    def __str__(self):
        return self.underlying.__name__

    def __call__(self, morph_func=None, /, **params):
        # params = self.prep_params()

        PARAMS_ARE_PASSED_IN = bool(params)  # defer params validity checking down the road
        PARAMS_ARE_FULLY_INITIALIZED = self.params.fields_init_status == "full"
        PARAMS_ARE_SUBSET_OF_SEMANTIC_TYPE_PARAMS = True  # this is guaranteed by validations performed in STRO.__call__
        PARAMS_ARE_READY = PARAMS_ARE_FULLY_INITIALIZED or PARAMS_ARE_SUBSET_OF_SEMANTIC_TYPE_PARAMS

        match (PARAMS_ARE_PASSED_IN, PARAMS_ARE_READY):
            case (False, True):
                # ACTUAL CALL MODE, return the actual type representation (e.g. pa.int64() etc.)
                # Under current design, (STRO.underlying: Callable) is allowed to have less parameter than its semantic type (i.e. STRO.semantic_type). When this is the case, there are a few possible strategies to handle the case:
                #   1. ignore the extra parameters of STRO.semantic_type
                #   2. raise Exception

                params_dict = self.params.dict_non_recurse(exclude_none=True)
                return self.underlying(**params_dict)
            case (False, False):
                raise AssertionError("Params are not fully initialized.")
            case (True, _):
                # BINDING-PARAMS MODE

                # It seems that params.copy(update=...) does not respect the `extra=Extra.Forbid` configuration, therefore a manual validation is needed
                assert set(params) <= set(self.params.__fields__)

                # Overrides the current params
                params = self.params.copy(update=params)
                # transitively update self.semantic_type.params
                semantic_type = self.semantic_type.copy(update={"params": params})

                new_morphism = self.copy(update={"params": params, "semantic_type": semantic_type})
                return new_morphism
            case _:
                raise ValueError

    @property
    def merged_params(self) -> MorphParams:
        """merge params with default_params"""
        if self.params.fields_init_status == "full":
            return self.params

        # self.params takes precedence over self.default_params
        params = self.default_params.copy(update=self.params.dict_non_recurse(exclude_none=True))
        return params

    def cast(self):
        """
        Cast higher level CTRO to level 1 CTRO, using default_params for unset param fields.
        :return:
        """
        return self.copy(update={"params": self.merged_params})()
