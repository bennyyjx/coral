from __future__ import annotations

from abc import ABC, abstractmethod
from pathlib import Path

from typing import Type, ClassVar

from enum import Enum
from uuid import UUID, uuid4

from typing_extensions import Self, Literal
from pydantic import BaseModel, Field, validator, Extra


class CoralBaseModel(BaseModel):
    _uid: UUID = Field(default_factory=uuid4)

    class Config:
        underscore_attrs_are_private = True
        extra = Extra.forbid

    def __hash__(self):
        """
        Note that the hash value will survive self.copy();
        :return:
        """
        return hash(self._uid)

    def dict_non_recurse(self, exclude_none: bool = False) -> dict:
        _dict = dict(self)
        if exclude_none:
            _dict = {
                k: v for k, v in _dict.items() if v is not None
            }
        return _dict


class Attr(CoralBaseModel):
    """..."""
    name: str


class SemanticType(CoralBaseModel):
    ndim: Literal[0, 1, 2]
    nlevel: Literal[1, 2, 3] = None
    type_system: Enum | None = None
    nullable: bool | None = None
    _parameterizable: bool | None = None
    morph_params: tuple | None = None  # type constructor parameters
    sortable: bool | None = None

    class Config:
        underscore_attrs_are_private = True

    def __init__(self, **data):
        super().__init__(**data)
        self._parameterizable = True if self.morph_params else False

    def lower_dim_type(self) -> Type[Self]:
        ...


class TypeSystemKind(Enum):
    # TODO: use pydantic or attrs to "simulate" an Enum
    #   getting rid of python builtin Enum's quirkiness
    #   while retaining it's ability to hint IDE for attributes
    DB = "DB"
    PY = "PY"
    XL = "Cross Language"


class TypeSystemAttrs(CoralBaseModel):
    name: str
    kind: TypeSystemKind
    aliases: set[str] = Field(default_factory=set)
    description: str = ''

    def __str__(self):
        return self.name


class TypeSystem(CoralBaseModel):
    """
    TypeSystem is a special kind of TypeReprSystem that denotes the "canonical" representation of an actual type system specification.

    For type system of a database, the canonical representation uses string, whereas for other type systems whose official python implementation is available (e.g. arrow -> pyarrow, numpy -> numpy, polars -> polars etc.), the canonical representation should generally use the Python constructs(class object, class instance etc.) supplied by the implementation.

    """
    # TODO: should this class be renamed "CanonicalTypeSystem"?
    kind: TypeSystemKind


class TypeReprSystem(CoralBaseModel):
    name: str
    description: str = ''
    # ts: set[TypeSystem] = Field(default_factory=set)


class CanonicalTypeReprSystem(TypeReprSystem):
    """
    """


CR = TypeReprSystem(name="Coral")


class StringImpl(CoralBaseModel, ABC):
    """A data type's string representation."""
    repr: str
    canonical_data: ClassVar[list[tuple[str, ...]]] = None
    canonical_data_schema: ClassVar[tuple[str, ...]] = None

    @validator("repr")
    @abstractmethod
    def check_value_against_canonical_data(cls, v):
        ...

    def __init_subclass__(cls, canonical_data_path: Path, **kwargs):
        import csv
        assert canonical_data_path.suffix == '.csv'
        # Also assumes that first row is header
        with open(canonical_data_path, mode='r') as f:
            reader = csv.reader(f)
            cls.canonical_data_schema = tuple(next(reader))
            cls.canonical_data = [tuple(row) for row in reader]
