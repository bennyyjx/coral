from coral.builtin.type_systems import CH, PG
from coral.builtin.type_repr_systems import PA, CC
from coral.builtin.repr_implementations import repr_implementations
from coral.core.base import TypeSystem, TypeReprSystem, CR
from coral.core.hub import DirectMapper, PivotMapper, HomoDirectMapper

system_mapper = DirectMapper[TypeSystem, TypeReprSystem].build(
    lefts=(CH, PG),
    rights=(PA, CC),
    links=repr_implementations
)

from coral.core.coral import CoralType
from pyarrow import DataType as PaType
from clickhouse_connect.datatypes.base import ClickHouseType as ChType

cr_pa_type_mapper = DirectMapper[CoralType, PaType].build()
cr_ch_type_mapper = DirectMapper[CoralType, ChType].build()

pa_ch_type_mapper = PivotMapper[PaType, ChType, CoralType]

hmapper1 = HomoDirectMapper[TypeReprSystem].build(
    lefts=(PA, CR),
    rights=(CC,)
)

breakpoint()

# TypeReprSystemHub_()
