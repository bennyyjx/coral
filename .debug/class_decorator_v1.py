from __future__ import annotations
from pydantic import BaseModel, validator, root_validator
import pyarrow as pa
from inspect import Signature, signature
from typing_extensions import Self
from typing import Callable, Literal, Any, TypeAlias
from coral.core.base import CoralBaseModel

TypeConstructor: TypeAlias = Callable


class TypeParams(CoralBaseModel):
    """
    Use this class's instances to generate a textual representation of TypedDict.
    The TypedDict class object can be used to annotate type class constructor's `kwargs` argument;
    This follows PEP692 with mypy support.
    To safely support such code generation behavior, additional qa tests should be placed in the test suite and run by CI / pre-commit hook.
    ref: https://github.com/python/mypy/issues/4441
    ref: https://peps.python.org/pep-0692/#reference-implementation
    """

    class Config:
        validate_assignment = True

    @property
    def attrs_init_status(self) -> Literal['full', 'partial', 'none']:
        attrs_inited = [field is not None for field in self.__dict__.values()]

        if all(attrs_inited):
            return "full"
        if any(attrs_inited):
            return "partial"
        return "none"


# for a given pair of (func: TypeConstructor, Tp: TypeParams), the following constraint holds:
#   inspect.signature(func).parameters === Tp

class Morphism(BaseModel):
    semantic_type: MetaMorphism
    underlying: TypeConstructor
    param_values: TypeParams = None

    class Config:
        underscore_attrs_are_private = True
    @root_validator
    def init_param_values(cls, values: dict):
        param_values = values.get("params")
        if not param_values:
            semantic_type: MetaMorphism = values['semantic_type']
            default_factory: type[TypeParams] = semantic_type.underlying
            values.update(param_values=default_factory())
        return values

    def __init__(self, underlying, /, **params):
        super().__init__(underlying=underlying, **params)

    def __call__(self, morph_func=None, /, **params):
        #  Morphism的underlying是真正的type_constructor，被call后输出真正的type repr (e.g. pa.int64()等)，因此__call__有三个用途，分别是：
        #   1. 在装饰别人的同时传入了参数，导致init-not-complete, 因此借用__call__来完成最后的initialization, 此时的morph_func必须不能是None,返回一个Morphism;
        #   2. 被call，此时的morph_func必须是None, params可以是空，但传进来以后需要根据level等其他信息further validate

        # 1. self. param_values.is_fully_initialized, 调用真正的type constructor

        PARAMS_ARE_PASSED_IN = bool(params)  # defer checking params validity down the road
        PARAMS_ARE_FULLY_INITIALIZED = self.param_values.attrs_init_status == "full"

        match (PARAMS_ARE_PASSED_IN, PARAMS_ARE_FULLY_INITIALIZED):
            case (False, True):
                return self.underlying(**self.param_values.dict())
            case (False, False):
                return self
            case (True, _):
                # Overrides the current param_values
                # TODO: self.semantic_type.param_values should be transitively updated.
                param_values = self.param_values.copy(update=params)
                new_morphism = self.copy(update={"params": param_values})
                return new_morphism
            case _:
                raise ValueError


class MetaMorphism(BaseModel):
    """
    作为Decorator，不接受除underlying之外的额外参数
    """
    semantic_type: None = None
    underlying: type[TypeParams]
    default_values: TypeParams  = None  # used during mapping (e.g. ch_lowcard(val_type) -> pa_dict(idx_type, val_type, ordered)

    class Config:
        underscore_attrs_are_private = True
        # arbitrary_types_allowed = True

    def __init__(self, underlying, /, **kwargs):
        super().__init__(underlying=underlying, **kwargs)

    # TODO: make morph_func typing more specific
    def __call__(self, morph_func: Callable, **kwargs) -> Morphism | MetaMorphism:
        #  MetaMorphism的underlying是dummy，不需要被call，因此__call__的唯二用途是：
        #   1. 在传入参数值的情况下完成自身的initialization, 此时的morph_func是coral_*, 返回一个MetaMorphism;
        #   2. 装饰别的callable，此时的morph_func是pa_*, ch_* etc.,返回一个Morphism
        #   例如：
        """
        @CoralInt(signed=True)
        def ch_int(bit_len: int):
            ...
        """
        if kwargs:
            # kwargs是MetaMorphism instance作为decorator时接收的额外参数，产生一个新的MetaMorphism instance
            return self.__class__(self.underlying, default_values=self.underlying(**kwargs))

        # 没有kwargs传入，则__call__作为decorator使用
        return Morphism(morph_func, semantic_type=self)

    @property
    def name(self):
        return self.underlying.__name__


Morphism.update_forward_refs()

# CoralInt = MetaMorphism(underlying=_coral_int, sig=signature(_coral_int))

@MetaMorphism
class CoralInt(TypeParams):
    # TODO: the reason that the model fields has default value of "None" is to allow
    #   partial application / incremental building / ..., essentially this class
    bit_len: int = None
    signed: bool = None

@CoralInt
def PaInt(bit_len: int, signed: True):
    match (signed, bit_len):
        case (True, 8): return pa.int8()
        case (True, 16): return pa.int16()
        case (True, 32): return pa.int32()
        case (True, 64): return pa.int64()
        case (False, 8): return pa.uint8()
        case (False, 16): return pa.uint16()
        case (False, 32): return pa.uint32()
        case (False, 64): return pa.uint64()
        case _:
            raise ValueError



PaSignedInt = PaInt(signed=True)
PaInt64 = PaInt(bit_len=64, signed=True)
PaInt64_alt = PaSignedInt(bit_len=64)
print(PaInt64())
print(PaInt64())
breakpoint()
