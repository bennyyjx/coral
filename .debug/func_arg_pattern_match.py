from typing import Callable

from pydantic import BaseModel


def zz():
    ...
class Foo(BaseModel):

    def bar(self, mf: int = 1, /, *, tro: str, **kwargs):
        return str(mf) + tro

    def baz(self, mf: Callable = None, /, *, tro: type[BaseModel], **kwargs):
        match mf, kwargs:
            case None, {**extra} if not extra:
                print("mf: None, kwargs: empty")
            case None, {}:
                print(f"mf: None, kwargs:{kwargs}")
            case func, {**extra} if (not extra) and (func is not None):
                print(f"mf: some callable, kwargs: {kwargs}")
            case _:
                print("Other cases")
        return

    def lorem(self, mf: Callable = None, /, *, tro: type[BaseModel], **kwargs):
        match mf, kwargs:
            case None, _:
                print("mf is None")
            case f, _:
                print(f"mf is {f}")

if __name__ == '__main__':
    f1 = Foo()
    try:
        f1.bar(5)  # runtime error: missing required kwonly arg: tro
    except Exception as e:
        print(e)

    x = f1.bar(tro='abc')  # OK

    f1.baz(tro=Foo)
    f1.baz(tro=Foo, a=1, b=2)
    f1.baz(zz, tro=Foo, c=3)
    f1.baz(zz, tro=Foo)

    f1.lorem(tro=Foo, lorem=3)
    f1.lorem(zz, tro=Foo, lorem=3)
    f1.lorem(zz, tro=Foo)

    breakpoint()
