from __future__ import annotations

from abc import ABC
from functools import partial
from inspect import signature

from pydantic import BaseModel, root_validator, Extra
import pyarrow as pa
from typing import Callable, Literal, Any, TypeAlias, ClassVar

from coral.builtin.type_repr_systems import PA
from coral.core.base import CoralBaseModel, TypeReprSystem, CR


class MorphParams(CoralBaseModel):
    """
    Use this class's instances to generate a textual representation of TypedDict.
    The TypedDict class object can be used to annotate type class constructor's `kwargs` argument;
    This follows PEP692 with mypy support.
    To safely support such code generation behavior, additional qa tests should be placed in the test suite and run by CI / pre-commit hook.
    ref: https://github.com/python/mypy/issues/4441
    ref: https://peps.python.org/pep-0692/#reference-implementation
    """

    class Config:
        validate_assignment = True

    @classmethod
    def compatible_with(cls, func: Callable) -> bool:
        from pydantic.decorator import (
            ValidatedFunction,
            ALT_V_ARGS,
            ALT_V_KWARGS,
            V_POSITIONAL_ONLY_NAME,
            V_DUPLICATE_KWARGS,
        )
        from pydantic import ConfigDict

        # ref: pydantic.decorator.ValidatedFunction.__init__
        excluded_field_names = {
            ALT_V_ARGS, ALT_V_KWARGS, V_POSITIONAL_ONLY_NAME, V_DUPLICATE_KWARGS, "args", "kwargs"
        }

        func_model: BaseModel = ValidatedFunction(func, config=ConfigDict(arbitrary_types_allowed=True)).model
        self_fields = {(field.name, field.type_) for field in cls.__fields__.values()}
        func_fields = {
            (field.name, field.type_) for field in func_model.__fields__.values()
            if field.name not in excluded_field_names
        }

        # TODO: properly implement compatibility check.
        #   currently only checks param names, due to additional complexities when the argument represents a type rather than a value
        #   e.g. PaDictionary(value_type: pa.DataType, ...) vs. PaInt(bit_len: int, signed: bool)
        #                    type __↑                             value __↑   value __↑
        # is_compatible = func_fields <= self_fields
        is_compatible = {f[0] for f in func_fields} <= {f[0] for f in self_fields}

        return is_compatible

    @property
    def fields_init_status(self) -> Literal['full', 'partial', 'none']:
        attrs_inited = [field is not None for field in self.__dict__.values()]

        if all(attrs_inited):
            return "full"
        if any(attrs_inited):
            return "partial"
        return "none"


class TypeReprObject(CoralBaseModel, ABC):
    repr_system: ClassVar[TypeReprSystem]
    semantic_type: SemanticTypeReprObject | None
    underlying: Any  # TODO: type narrowing

    class Config:
        underscore_attrs_are_private = True
        extra = Extra.forbid


class SemanticTypeReprObject(TypeReprObject, ABC):
    # name: str
    semantic_type: None = None
    underlying: type[MorphParams]
    default_values: MorphParams = None  # used during mapping (e.g. ch_lowcard(val_type) -> pa_dict(idx_type, val_type, ordered)

    _called_without_morph_func: bool = False

    def __init__(self, underlying, /, **kwargs):
        super().__init__(underlying=underlying, **kwargs)

    def __init_subclass__(cls, repr_system: TypeReprSystem, **kwargs):
        super().__init_subclass__(**kwargs)
        cls.repr_system = repr_system

    def __call__(self, morph_func: Callable = None, /, *, tro_cls: type[ConcreteTypeReprObject], **kwargs):
        #  MetaMorphism的underlying是dummy，不需要被call，因此__call__的唯二用途是：
        #   1. 在传入参数值的情况下完成自身的initialization, 此时的morph_func是coral_*, 返回一个MetaMorphism;
        #   2. 装饰别的callable，此时的morph_func是pa_*, ch_* etc.,返回一个Morphism
        #   例如：
        """
        @CoralInt(signed=True)
        def ch_int(bit_len: int):
            ...
        """
        match morph_func, kwargs, self._called_without_morph_func:
            case None, _, True:
                # error case:
                raise ValueError(
                    "You have called the object twice without sepcifying the morph_func argument, which is not allowed.")
            case None, _, False:
                """
                1. Save morph_func, kwargs to a temporary place
                2. record call stack depth (increment an int variable)
                3. return new CoralInt, expect to enter "morph_func is not None" branch
                """
                # mutating the current class will cause error when decoration happens the second time.
                # self._called_without_morph_func = True
                # return partial(self.__call__, tro_cls=tro_cls, **kwargs)
                new_stro = self.copy()
                new_stro._called_without_morph_func = True
                return partial(new_stro.__call__, tro_cls=tro_cls, **kwargs)

            case func, _, _:
                # Validation rules:
                #   0. func parameters cannot have default values
                #   1. signature(func).parameters compatible with self.underlying
                #   2. kwargs.keys() and  signature(func).parameters.keys() are:
                #   2.1 disjoint, and
                #   2.2 the union of the two equals self.underlying

                func_params_has_default_values = tuple(p.default is p.empty for p in signature(func).parameters.values())
                if any(func_params_has_default_values):
                    raise AssertionError("func parameters cannot have default values")

                kwargs_names = set(kwargs.keys())
                func_params_names = set(func.__annotations__.keys())
                self_params_names = set(self.underlying.__fields__.keys())

                match (
                    self.underlying.compatible_with(func),
                    kwargs_names.isdisjoint(func_params_names),
                    (kwargs_names | func_params_names) == self_params_names
                ):
                    case True, True, True:
                        self.default_values=self.underlying(**kwargs)
                        return tro_cls(func, semantic_type=self)
                    case _compatible, _is_disjoint, _union_equals:
                        # TODO: specify error messages based on matched bool values
                        raise AssertionError
            case _:
                raise ValueError


class ConcreteTypeReprObject(TypeReprObject, ABC):
    semantic_type: SemanticTypeReprObject
    underlying: TypeConstructor
    params: MorphParams = None

    @root_validator
    def init_param_values(cls, values: dict):
        param_values = values.get("params")
        if not param_values:
            semantic_type: SemanticTypeReprObject = values['semantic_type']
            default_factory: type[MorphParams] = semantic_type.underlying
            values.update(param_values=default_factory())
        return values

    def __init__(self, underlying, /, **params):
        super().__init__(underlying=underlying, **params)

    def __init_subclass__(cls, repr_system: TypeReprSystem, **kwargs):
        super().__init_subclass__(**kwargs)
        cls.repr_system = repr_system

    def __call__(self, morph_func=None, /, **params):
        PARAMS_ARE_PASSED_IN = bool(params)  # defer checking params validity down the road
        PARAMS_ARE_FULLY_INITIALIZED = self.params.fields_init_status == "full"

        match (PARAMS_ARE_PASSED_IN, PARAMS_ARE_FULLY_INITIALIZED):
            case (False, True):
                return self.underlying(**self.params.dict())
            case (False, False):
                return self
            case (True, _):
                # Overrides the current param_values
                # TODO: self.semantic_type.param_values should be transitively updated.
                param_values = self.params.copy(update=params)
                new_morphism = self.copy(update={"params": param_values})
                return new_morphism
            case _:
                raise ValueError


TypeConstructor: TypeAlias = Callable


class CoralType(SemanticTypeReprObject, repr_system=CR):  # formerly CoralType
    """
    作为Decorator，不接受除underlying之外的额外参数
    """

    # TODO: make morph_func typing more specific
    @property
    def name(self):
        return self.underlying.__name__


if __name__ == '__main__':
    @CoralType
    class CoralInt(MorphParams):
        # TODO: the reason that the model fields has default value of "None" is to allow
        #   partial application / incremental building / ..., essentially this class
        bit_len: int = None
        signed: bool = None


    @CoralType
    class CoralDictionary(MorphParams):
        index_type: SemanticTypeReprObject = None
        value_type: SemanticTypeReprObject = None
        ordered: bool = None

    @CoralType
    class CoralVarString(MorphParams):
        is_large: bool = None

    class PaType(ConcreteTypeReprObject, repr_system=PA):
        """..."""


    # candidate 1:
    #   pros: succinct, intuitive
    #   cons: lost repr_system information
    # @CoralInt
    # def PaInt(bit_len: int, signed: True):
    #     match (signed, bit_len):
    #         case (True, 8): return pa.int8()
    #         case (True, 16): return pa.int16()
    #         case (True, 32): return pa.int32()
    #         case (True, 64): return pa.int64()
    #         case (False, 8): return pa.uint8()
    #         case (False, 16): return pa.uint16()
    #         case (False, 32): return pa.uint32()
    #         case (False, 64): return pa.uint64()
    #         case _:
    #             raise ValueError
    #
    # # candidate 2:
    # #   pros: keeps repr_system information
    # #   cons: looks a bit awkward, logic in CoralType.__call__ does not seem right (consistent)
    # @CoralInt
    # class PaFloat(PaType):
    #     def __call__(self, bit_len: int):
    #         if bit_len == 16:
    #             return pa.float16()
    #         elif bit_len == 32:
    #             return pa.float16()
    #         elif bit_len == 64:
    #             return pa.float16()
    #         else:
    #             raise ValueError

    # candidate 3:
    #   pros: keeps repr_system information
    #   cons: looks a bit awkward, logic in CoralType.__call__ does not seem right (consistent)
    @CoralInt(tro_cls=PaType)
    def PaInt(bit_len: int, signed: bool = False):
        match signed, bit_len:
            case (True, 8):
                return pa.int8()
            case (True, 16):
                return pa.int16()
            case (True, 32):
                return pa.int32()
            case (True, 64):
                return pa.int64()
            case (False, 8):
                return pa.uint8()
            case (False, 16):
                return pa.uint16()
            case (False, 32):
                return pa.uint32()
            case (False, 64):
                return pa.uint64()
            case _:
                raise ValueError


    @CoralDictionary(tro_cls=PaType, index_type=CoralInt, ordered=False)
    def PaLowCard(value_type: pa.DataType):
        return pa.dictionary(pa.int16(), pa.string(), False)

    @CoralVarString(tro_cls=PaType, is_large=False)
    def PaString():
        return pa.string()

    PaSignedInt = PaInt(signed=True)
    PaInt64 = PaInt(bit_len=64, signed=True)
    PaInt64_alt = PaSignedInt(bit_len=64)
    print(PaInt64())
    print(PaInt64())

    breakpoint()
