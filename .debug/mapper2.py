import coral.builtin.maps.pa as maps_pa
import coral.builtin.maps.ch as maps_ch
from coral.builtin.trs.clickhouse import ChFloat64, ChUInt8, ChInt32, ChFloat32
from coral.core.coral import CoralBool, CoralInt64, CoralFloat32, CoralTimestamp
from coral.builtin.trs.pyarrow import PaDictionary, PaLargeString, PaFloat64, PaBool
from coral.core.hub import HomoDirectMapper
from coral.core.repr import TypeReprObject

import networkx as nx


mapper_pa = HomoDirectMapper[TypeReprObject].build(
    lefts=(CoralBool, CoralInt64, CoralFloat32, CoralTimestamp),
    rights=(PaBool, PaFloat64, PaLargeString, PaDictionary),
    links=maps_pa.repr_map
)

mapper_ch = HomoDirectMapper[TypeReprObject].build(
    lefts=(CoralBool, CoralInt64, CoralFloat32, CoralTimestamp),
    rights=(ChUInt8, ChFloat64, ChInt32, ChFloat32),
    links=maps_ch.repr_map
)

str(PaBool)
repr(ChUInt8)


G = nx.compose(mapper_pa.mapping, mapper_ch.mapping)

breakpoint()
