import uuid

import networkx as nx
from pydantic import BaseModel, Field


class C(BaseModel):
    a: int
    b: bool

class SemanticTypeReprObject(BaseModel):
    # name: str
    semantic_type: None = None
    underlying: type[C]
    default_values: C = None  # used during mapping (e.g. ch_lowcard(val_type) -> pa_dict(idx_type, val_type, ordered)

    _called_without_morph_func: bool = False
    _uid: uuid.UUID = Field(default_factory=uuid.uuid4)
    class Config:
        underscore_attrs_are_private = True
    def __hash__(self):
        return hash(self._uid)

    def __repr__(self):
        return f"{self.__class__.__name__}{self._uid}"

    def __str__(self):
        return f"{self.__class__.__name__}{self._uid}"

G1 = nx.Graph()

stro_v1_1 = SemanticTypeReprObject(underlying=C)
stro_v1_2 = stro_v1_1.copy()
stro_v1_2._called_without_morph_func = True

G1.add_edges_from([(1, stro_v1_1), (1, stro_v1_2)])

print(f"{len(G1.nodes) =}")


class SemanticTypeReprObjectV2(SemanticTypeReprObject):
    def __hash__(self):
        return hash(self.__dict__)


G2 = nx.Graph()

stro_v2_1 = SemanticTypeReprObjectV2(underlying=C)
stro_v2_2 = stro_v2_1.copy()
stro_v2_2._called_without_morph_func = True

G2.add_edges_from([(1, stro_v2_1), (1, stro_v2_2)])

print(f"{len(G2.nodes) =}")
