import coral.core.coral as cr
from coral.lib import transform_cr_to_ch

# ch_varstr_str = transform_cr_to_ch_v2(from_=cr.CoralFixedNumber(precision=10, scale=3))
ch_varstr_str = transform_cr_to_ch(from_=cr.CoralDictionary(value_type=cr.CoralVarString))

print(ch_varstr_str())

# breakpoint()

