from coral.core.repr import MorphParams

class MP(MorphParams):
    a: int = 1
    b: bool = 2

    def __repr__(self):
        return "abc"

    def __str__(self):
        return "bcd"

d = MP()

print(d)
print(str(d))
print(repr(d))

breakpoint()
