from functools import partial
from inspect import signature, Signature, Parameter, BoundArguments


def foo(a: int, b: bool) -> str:
    return str(a)

sig = signature(foo)

bound_args_1 = sig.bind(a=5, b=False)
bound_args_2 = sig.bind(a=5, b=3)

breakpoint()

