import dataclasses
import enum
from inspect import signature
from coral.core.repr import TypeReprObject
from coral.core.coral import CoralType


def foo1(a: CoralType, b: CoralType, c: bool):
    ...


def foo2(a: TypeReprObject, b: TypeReprObject):
    ...


sig1 = signature(foo1)
sig2 = signature(foo2)

sig1_params = dict(sig1.parameters)
sig2_params = dict(sig2.parameters)


print()
