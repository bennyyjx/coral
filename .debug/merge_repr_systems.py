from coral.builtin.trs.clickhouse import ClickhouseStrTypeReprSystem, ChType
from coral.builtin.trs.pyarrow import PyArrowTypeReprSystem, PaType
from coral.core.coral import CoralTypeReprSystem
import networkx as nx

G = nx.compose_all([ClickhouseStrTypeReprSystem, PyArrowTypeReprSystem, CoralTypeReprSystem])

breakpoint()

from coral.core.coral import CoralInt, CoralUnsignedInt, CoralUInt8

from pprint import pprint

pprint(list(G.predecessors(CoralInt)))
pprint(list(G.predecessors(CoralUnsignedInt)))
pprint(list(G.predecessors(CoralUInt8)))

breakpoint()

print("Predecessors: ")
for cr_node in CoralTypeReprSystem.nodes:
    print(f"{cr_node}: {list(CoralTypeReprSystem.predecessors(cr_node))}")

print("Successors: ")
for pa_node in PyArrowTypeReprSystem.nodes:
    print(f"{pa_node}: {list(PyArrowTypeReprSystem.successors(pa_node))}")
